const Koa = require('koa');
const KoaBody = require('koa-body');
const KoaRouter = require('koa-router');
const KoaCors = require('@koa/cors');
const koaRequest = require('koa-http-request');

const log4js = require("log4js")
log4js.configure({
    appenders: {cheese: {type: "file", filename: "log_file.log"}},
    categories: {default: {appenders: ["cheese"], level: "error"}}
})

const logger = log4js.getLogger("cheese")
logger.error("Cheese is too ripe!")

// TODO подключить лог
const app = new Koa();
const router = new KoaRouter();

const pool = require('./mysql')

let options = {
    origin: false,
    credentials: true,
}
const mtBody = {
    'User-Agent': 'koa-http-request',
    'Authorization': 'Bearer 5fa625d39276a08ef9a6463f3f467abedeabc54db7923404f737c10256fc77ef',
};

app.use(KoaCors(options));

app.use(koaRequest({
    json: true,
    timeout: 3000,
    host: 'https://www.meistertask.com'
}));

//meistertask api
router.get("/api/projects", async (ctx, next) => {
    ctx.body = await ctx.get('/api/projects?status=1', null, mtBody);
});
router.get("/api/tasks/:taskID/task_labels", async (ctx, next) => {
    ctx.body = await ctx.get('/api/tasks/' + ctx.params.taskID + '/task_labels', null, mtBody);
});
router.get("/api/projects/:projectId/persons", async (ctx, next) => {
    let projectInfo = {};
    projectInfo['persons'] = await ctx.get('/api/projects/' + ctx.params.projectId + '/persons', null, mtBody);
    ctx.body = projectInfo;
});
router.get("/api/projects/:projectId/:userId/tasks", async (ctx, next) => {
    let projectInfo = {};
    let projectIntervals = await ctx.get('/api/projects/' + ctx.params.projectId + '/work_intervals', null, mtBody);
    let taskIntervals = {}
    projectInfo['tasks'] = await ctx.get('/api/projects/' + ctx.params.projectId + '/tasks', null, mtBody);
    projectInfo['labels'] = await ctx.get('/api/projects/' + ctx.params.projectId + '/labels', null, mtBody);
    for (const intervalIndex in projectIntervals) {
        if (taskIntervals[projectIntervals[intervalIndex]['task_id']] === undefined) {
            taskIntervals[projectIntervals[intervalIndex]['task_id']] = [projectIntervals[intervalIndex]];
        } else {
            taskIntervals[projectIntervals[intervalIndex]['task_id']].push(projectIntervals[intervalIndex])
        }
    }
    projectInfo['intervals'] = taskIntervals;
    projectInfo['label'] = [];

    Object.filter = (obj, predicate) =>
        Object.keys(obj)
            .filter(key => predicate(obj[key]))
            .reduce((res, key) => (res[key] = obj[key], res), {});

    for (const task in projectInfo['tasks']) {
        projectInfo['tasks'][task]['taskInfo'] = {};
        projectInfo['tasks'][task]['tracked_time_converted'] = 0;
        projectInfo['tasks'][task]['active'] = true;

        projectInfo['tasks'][task]['taskInfo']['intervals'] = taskIntervals[projectInfo['tasks'][task]['id']]
        projectInfo['tasks'][task]['taskInfo']['label'] = []
        projectInfo['tasks'][task]['taskInfo']['labels'] = []

        if (projectInfo['tasks'][task]['taskInfo']['intervals'] !== undefined) {
            projectInfo['tasks'][task]['taskInfo']['intervals'] = Object.filter(projectInfo['tasks'][task]['taskInfo']['intervals'],
                interv => parseInt(interv.person_id) === parseInt(ctx.params.userId));
        }

        for (const interval in projectInfo['tasks'][task]['taskInfo']['intervals']) {
            if (parseInt(projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['person_id']) === parseInt(ctx.params.userId)) {
                projectInfo['tasks'][task]['assigned_to_id'] = projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['person_id']

                let bitsFinish = projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['finished_at'].split(/\D/);
                let bitsStart = projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['started_at'].split(/\D/);
                let dateFinish = new Date(bitsFinish[0], bitsFinish[1], bitsFinish[2], bitsFinish[3], bitsFinish[4], bitsFinish[5]);
                projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['finish_date'] = bitsFinish[2] + '.' + bitsFinish[1] + '.' + bitsFinish[0];
                projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['ar_finish_date'] = bitsFinish[0] + '-' + bitsFinish[1] + '-' + bitsFinish[2];
                let dateStart = new Date(bitsStart[0], bitsStart[1], bitsStart[2], bitsStart[3], bitsStart[4], bitsStart[5]);
                projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['dif_date'] = (dateFinish - dateStart) / 1000;

                projectInfo['tasks'][task]['tracked_time_converted'] += projectInfo['tasks'][task]['taskInfo']['intervals'][interval]['dif_date']
            }
        }
        projectInfo['tasks'][task]['tracked_time'] = projectInfo['tasks'][task]['tracked_time_converted']
        projectInfo['tasks'][task]['tracked_time_converted'] = sToHour(projectInfo['tasks'][task]['tracked_time_converted'])
    }

    projectInfo['tasks'] = Object.filter(projectInfo['tasks'], task => task.assigned_to_id === parseInt(ctx.params.userId));
    let labels = {};
    for (const label of projectInfo['labels']) {
        labels[label.id] = label;
    }
    projectInfo['labels'] = labels;
    ctx.body = projectInfo;
});
router.get("/api/task/:projectId/:userId/:taskId/info", async (ctx, next) => {
    let taskInfo = {};
    let taskLabel = await ctx.get('/api/tasks/' + ctx.params.taskId + '/task_labels', null, mtBody);
    taskInfo['label'] = taskLabel;
    taskInfo['labels'] = {};
    for (const label in taskInfo['label']) {
        taskInfo['labels'][taskInfo['label'][label]['label_id']] = taskInfo['label'][label];
    }
    ctx.body = taskInfo;
})

//database
router.get("/db/projects/:projectId/:userId/:labelId/tasks", async (ctx, next) => {
    let requestStr = "";
    if (ctx.params.labelId === 'all_labels') {
        requestStr = "SELECT * from tasks WHERE project_id = '" + ctx.params.projectId + "' AND user_id = '" + ctx.params.userId + "'";
    } else {
        requestStr = "SELECT * from tasks WHERE project_id = '" + ctx.params.projectId + "' AND user_id = '" + ctx.params.userId + "' AND label_id='" + ctx.params.labelId + "'";
    }
    let arResult = {
        tasks: [],
        general_time: 0,
        end_date: []
    };
    await pool
        .execute(requestStr)
        .then(([rows]) => {
            arResult['general_time'] = 0;
            for (const rowIndex in rows) {
                rows[rowIndex]['edit'] = false
                if (rows[rowIndex]['plan_time'] > 0) {
                    arResult.general_time += parseFloat(rows[rowIndex]['plan_time'])
                }
                if (rows[rowIndex]['plan_date'] !== '') {
                    arResult.end_date.push(rows[rowIndex]['plan_date'])
                }
            }
            arResult.tasks = rows;
            ctx.body = arResult;
        })
        .catch(function (err) {
            console.log(err.message);
        });
});
router.put("/db/update/projects/:projectId/:userId/tasks", KoaBody(), async (ctx, next) => {
    let row = ctx.request.body;
    let rowId = null;
    let dbUpdateString = '';
    let dbInsertStringFields = '';
    let dbInsertStringValues = '';
    let k = 0;
    for (const [rowField, value] of Object.entries(row)) {
        k++;
        if (rowField === 'id') {
            rowId = value;
        } else {
            dbUpdateString += rowField + " = " + "'" + value + "'";
            dbInsertStringFields += rowField;
            dbInsertStringValues += "'" + value + "'";
            if (k !== Object.keys(row).length && rowField !== 'id') {
                dbUpdateString += ', ';
                dbInsertStringFields += ', ';
                dbInsertStringValues += ', ';
            }
        }
    }
    if (rowId !== '') {
        await pool
            .execute("UPDATE tasks SET " + dbUpdateString + " WHERE id = " + rowId)
            .then(result => {
                ctx.body = result;
            })
            .catch(function (err) {
                console.log(err.message);
            });
    } else {
        await pool
            .execute("INSERT INTO tasks " +
                "(project_id, user_id, " + dbInsertStringFields + ") VALUES " +
                "('" + ctx.params.projectId + "', '" + ctx.params.userId + "', " + dbInsertStringValues + ")")
            .then(result => {
                ctx.body = result;
            })
            .catch(function (err) {
                console.log(err.message);
            });
    }
    ctx.status = 200;
});
router.put("/db/delete/projects/:projectId/:userId/tasks", KoaBody(), async (ctx, next) => {
    let row = ctx.request.body;
    await pool
        .execute("DELETE FROM tasks WHERE id=" + row.id)
        .then(result => {
            ctx.body = result
        })
        .catch(function (err) {
            ctx.body = err;
        })
    ctx.status = 200;
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(8081);

function msToHour(num) {
    let seconds = parseInt((num / 1000) % 60),
        minutes = parseInt((num / (1000 * 60)) % 60),
        hours = parseInt((num / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
}

function sToHour(num) {
    let hours = Math.floor(num / 60 / 60);
    let minutes = Math.floor(num / 60) - (hours * 60);
    let seconds = num % 60;
    return ((hours < 10) ? '0' : '') + hours + ':' + ((minutes < 10) ? '0' : '') + minutes + ':' + ((seconds < 10) ? '0' : '') + seconds;
}