import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import {library} from '@fortawesome/fontawesome-svg-core'
import {faTrash, faPen, faSave, faTimes, faPlus, faChevronUp, faChevronDown} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {config} from './config';
Vue.prototype.appConfig = config

library.add(faTrash, faPen, faSave, faTimes, faPlus, faChevronUp, faChevronDown)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
